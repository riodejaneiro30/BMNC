from django.apps import AppConfig


class BmncNewsConfig(AppConfig):
    name = 'bmnc_news'
