"""BMNC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
import bmnc_news.urls as bmnc_news
import bmnc_sign_up.urls as bmnc_sign_up
import bmnc_polling.urls as bmnc_polling
import bmnc_profile.urls as bmnc_profile
from django.views.generic.base import RedirectView

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^news/', include(bmnc_news,namespace='bmnc-news')),
    url(r'^home/', include(bmnc_sign_up,namespace='bmnc-home')),
    url(r'^polling/', include(bmnc_polling, namespace='bmnc-polling')),
    url(r'^profile/', include(bmnc_polling, namespace='bmnc-profile')),
    url(r'^$', RedirectView.as_view(url="/home/", permanent='true'),name='index')
]
