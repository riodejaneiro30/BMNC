from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt

cursor = connection.cursor()
response = {'author': 'BMNC14B'}
def index(request) :
    html = 'bmnc_sign_up/sign_up.html'
    return render(request, html, response)

def index2(request):
    html = 'bmnc_sign_up/cobacoba.html'
    return render(request, html, response)


#Validasi username
#apakah username sudah ada apa belum

@csrf_exempt
def validate_username(request):
    username = request.POST.get('username', None)
    print(username)
    cursor.execute('''    SELECT *
                        FROM NARASUMBER
                        WHERE username = %s''', [username])
    catch = cursor.fetchall()
    data = {
        'is_taken': (len(catch) > 0)
    }
    return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    nomor_identitas = request.POST.get('nomor_identitas', None)
    cursor.execute('''    SELECT *
                             FROM MAHASISWA
                             WHERE npm = %s''', [nomor_identitas])
    catch = cursor.fetchall()
    data = {
        'is_taken': (len(catch) > 0)
    }
    return JsonResponse(data)

@csrf_exempt
def validate_no_dosen(request):
    nomor_identitas = request.POST.get('nomor_identitas', None)
    cursor.execute('''    SELECT *
                             FROM DOSEN
                             WHERE nik_dosen = %s''', [nomor_identitas])
    catch = cursor.fetchall()
    data = {
        'is_taken': (len(catch) > 0)
    }
    return JsonResponse(data)


@csrf_exempt
def validate_no_staf(request):
    nomor_identitas = request.POST.get('nomor_identitas', None)
    cursor.execute('''    SELECT *
                             FROM STAF
                             WHERE nik_staf = %s''', [nomor_identitas])
    catch = cursor.fetchall()
    data = {
        'is_taken': (len(catch) > 0)
    }
    return JsonResponse(data)

@csrf_exempt
def validate_mahasiswa(request):
    username = request.POST.get('username', None)
    npm = request.POST.get('npm', None)
    print(username)
    cursor.execute('''    SELECT *
                        FROM NARASUMBER
                        WHERE username = %s''', [username])
    catch_username = cursor.fetchall()
    cursor.execute('''    SELECT *
                        FROM MAHASISWA
                        WHERE npm = %s''', [npm])
    catch_npm = cursor.fetchall()
    data = {
        'username_is_taken': (len(catch_username) > 0),
        'npm_is_taken' : (len(catch_npm) > 0)
    }
    return JsonResponse(data)

@csrf_exempt
def validate_dosen(request):
    username = request.POST.get('username', None)
    nik = request.POST.get('nik', None)
    print(username)
    cursor.execute('''    SELECT *
                        FROM NARASUMBER
                        WHERE username = %s''', [username])
    catch_username = cursor.fetchall()
    cursor.execute('''    SELECT *
                        FROM DOSEN
                        WHERE nik_dosen = %s''', [nik])
    catch_nik = cursor.fetchall()
    data = {
        'username_is_taken': (len(catch_username) > 0),
        'nik_is_taken' : (len(catch_nik) > 0)
    }
    return JsonResponse(data)

@csrf_exempt
def validate_staf(request):
    username = request.POST.get('username', None)
    nik = request.POST.get('nik', None)
    print(username)
    cursor.execute('''    SELECT *
                FROM NARASUMBER
                WHERE username = %s''', [username])
    catch_username = cursor.fetchall()
    cursor.execute('''    SELECT *
                FROM STAF
                WHERE nik_staf = %s''', [nik])
    catch_nik = cursor.fetchall()
    data = {
        'username_is_taken': (len(catch_username) > 0),
        'nik_is_taken' : (len(catch_nik) > 0)
    }
    return JsonResponse(data)

@csrf_exempt
def registrasi_narasumber(request):
    if request.method == "POST":
        role= request.POST['rolenarasumber']
        username= request.POST['usernamenarasumber']
        password= request.POST['passwordnarasumber']
        idnumber= request.POST['idnumbernarasumber']
        nama= request.POST['namanarasumber']
        tempat = request.POST['tempatlahirnarasumber']
        bdate = request.POST['tanggallahirnarasumber']
        email = request.POST['emailnarasumber']
        no_telp = 0
        if (request.POST['phonenarasumber'] == ""):
            no_telp = ""
        else :
            no_telp = request.POST['phonenarasumber']
        if (request.POST['rolenarasumber']=="Mahasiswa"):
            status = request.POST['statusnarasumber']
            iduniv = request.POST['idunivnarasumber']
            print(username)
            print(password)
            cursor.execute('''  SELECT MAX(id)
                                FROM NARASUMBER''')
            id_narasumber = cursor.fetchall()+1
            cursor.execute('''    INSERT INTO NARASUMBER VALUES ( %s, %s, %s, %s, %s, %d, %d, %d, %s, %s);''', [nama,email,tempat,bdate,no_telp,0,0,iduniv,username,password])
            cursor.execute('''    INSERT INTO MAHASISWA VALUES (%d, %s, %s);''',[id_narasumber,idnumber,status])
        elif (request.POST['rolenarasumber']=="Dosen"):
            iduniv = request.POST['idunivnarasumber']
            cursor.execute('''  SELECT MAX(id)
                                FROM NARASUMBER''')
            id_narasumber = cursor.fetchall()+1;
            cursor.execute('''    INSERT INTO NARASUMBER
                        VALUES ( %s, %s, %s, %s, %s, %d, %d, %d, %s, %s);''', [nama, email, tempat, bdate, no_telp, 0, 0, iduniv, username, password])
            cursor.execute('''    INSERT INTO DOSEN
                        VALUES (%d, %s, %s);''',[id_narasumber,idnumber,"KOSONG"],[])

        elif (request.POST['rolenarasumber']=="Staf"):
            iduniv = request.POST['idunivnarasumber']
            cursor.execute('''    SELECT MAX(id)
                                FROM NARASUMBER''')
            id_narasumber = cursor.fetchall()+1;
            cursor.execute('''    INSERT INTO NARASUMBER
                        VALUES ( %s, %s, %s, %s, %s, %d, %d, %d, %s, %s);''', [nama, email, tempat, bdate, no_telp, 0, 0, iduniv, username, password])
            cursor.execute('''    INSERT INTO STAF
                        VALUES (%d, %s, %s);''',[id_narasumber,idnumber,"KOSONG"],[])

        response = {}
        return HttpResponseRedirect(reverse('login:login'))
