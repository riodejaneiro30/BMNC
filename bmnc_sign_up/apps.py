from django.apps import AppConfig


class BmncSignUpConfig(AppConfig):
    name = 'bmnc_sign_up'
