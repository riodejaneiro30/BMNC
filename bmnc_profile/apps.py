from django.apps import AppConfig


class BmncProfileConfig(AppConfig):
    name = 'bmnc_profile'
