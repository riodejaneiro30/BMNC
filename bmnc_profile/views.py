from django.shortcuts import render
from django.db import connection
from collections import namedtuple

response = {'author': 'BMNC14B'}
cursor = connection.cursor()
    
def index(request):
	html = 'bmnc_profile/polling.html'	
	print('ke profile')
	return render(request, html, response)

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]